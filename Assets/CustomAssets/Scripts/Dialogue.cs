﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Text;
using UnityEngine.SceneManagement;

public class Dialogue : MonoBehaviour
{

    private GameObject dialouge;
    public Text textbox;
    public string filename;
    public Text nameHolder;
    public Image spriteHolder;
    public Image Arrow;
    public string mName;
    public bool dontClose = false;
    public bool enableOnEnter = false;
    private StringReader sr;
    private TextAsset ta;
    private ArrayList lines;
    private int position = 0;
    private Animator anim;
    private float lastTime;
    private bool end = false;

    // Use this for initialization
    void Start()
    {
        lastTime = Time.time;
        anim = GetComponent<Animator>();
        lines = new ArrayList();
        dialouge = Player.dialogue;
        foreach (Text box in dialouge.GetComponentsInChildren<Text>())
        {
            if (box.name.Equals("Name")) nameHolder = box;
            if (box.name.Equals("Text")) textbox = box;
        }

        foreach (Image image in dialouge.GetComponentsInChildren<Image>())
        {
            if (image.name.Equals("Image")) spriteHolder = image;
            if (image.name.Equals("Arrow")) Arrow = image;
        }
        if (dialouge.activeInHierarchy) dialouge.SetActive(false);
        readText(filename);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay2D(Collider2D col)
    {
        talk(col);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        talk(col);
    }

    void talk(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Action") || enableOnEnter)
            {
                specialMethods();
                if (lines.Count < 2)
                {
                    Arrow.enabled = false;
                }
                else {
                    Arrow.enabled = true;
                }

                if (Time.time - lastTime > 0.2)
                {
                    anim.SetBool("Talking", true);
                    dialouge.SetActive(true);
                    if (!end || dontClose)
                        loadText();
                    else {
                        Player.dialogue.SetActive(false);
                        end = false;
                        completeActions();
                    }
                    lastTime = Time.time;
                }

            }
        }
    }

    public void talk(string text, string name, int time)
    {
        mName = name;
        dialouge.SetActive(true);
        Sprite sprite = null;
        Arrow.enabled = false;

        //Including player: before a line will allow the player to speak
        if (text.Contains("player:"))
        {
            sprite = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().sprite;
            nameHolder.text = "G8";
            text = text.Substring(6);
        }
        else
        {
            sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
            nameHolder.text = name;
        }

        spriteHolder.sprite = sprite;
        textbox.text = text;
        StartCoroutine(wait(time));
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            position = 0;
            dialouge.SetActive(false);
            if(anim.GetBool("Talking"))
                anim.SetBool("Talking", false);
            
        }
    }

    private void readText(string location)
    {
        string line = "";

        try
        {

            ta = Resources.Load("DialougeText/" + location) as TextAsset;
            sr = new StringReader(ta.text);
            while ((line = sr.ReadLine()) != null)
            {
                lines.Add(line);
            }
        }
        catch
        {
            line = "I forgot what I was going to say(File not found)";
            lines.Add(line);
        }

    }

    private void loadText()
    {
            string text = lines[position] as string;
            Sprite sprite = null;

            //Including player: before a line will allow the player to speak
            if (text.Contains("player:"))
            {
                sprite = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().sprite;
                nameHolder.text = "G8";
                text = text.Substring(7);
            }
            else
            {
                sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
                nameHolder.text = mName;
            }

            spriteHolder.sprite = sprite;
            textbox.text = text;

            if (++position >= lines.Count)
            {
                end = true;
                position = 0;
            }
       
    }

    private IEnumerator wait(int time)
    {
        yield return new WaitForSeconds(time);
        dialouge.SetActive(false);
    }

    private void specialMethods() {
        if (mName.Equals("Chief")) {
            Player player = FindObjectOfType<Player>();
            if (player.amountOf("pork") >= 8) {
                player.removeItem("pork", 8);
                filename = "4.treeDone";
                position = 0;
                lines = new ArrayList();
                readText(filename);
            }
        }

        if (mName.Equals("Native Merchant")) {
            Player player = FindObjectOfType<Player>();
            if (player.amountOf("banana") >= 5)
            {
                player.removeItem("banana", 5);
                filename = "4.8done";
                position = 0;
                lines = new ArrayList();
                readText(filename);
            }
        }
    }

    private void completeActions() {
        if (filename.Equals("4.treeDone")) {
            PlayerAttack playerAttack = FindObjectOfType<PlayerAttack>();
            playerAttack.setWeapon("fists");
            GameManager gm = FindObjectOfType<GameManager>();
            gm.save();

            SceneManager.LoadScene("Ostrach");
        }

        if (filename.Equals("4.8done")) {
            PlayerAttack playerAttack = FindObjectOfType<PlayerAttack>();
            playerAttack.findWeapon("bow").setOwned(true);
        }
    }

}
