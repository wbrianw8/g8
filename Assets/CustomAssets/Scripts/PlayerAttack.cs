﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour {
	
	private bool attacking = false;
	
	private float attactTimer = 0;
	private float attackCd = 0.3f;
	
	public GameObject attackTrigger;
	private Animator anim;

    private int damage;

    private Weapon currentWeapon;
    private List<Weapon> weapons;

    public RuntimeAnimatorController daggerAnim;
    public RuntimeAnimatorController fistsAnim;
    public RuntimeAnimatorController gunAnim;
    public RuntimeAnimatorController bowAnim;
    public Sprite gunAmmo;
    public Sprite bowAmmo;
    private Weapon dagger;
    private Weapon fists;
    private Weapon gun;
    private Weapon bow;

    private GameObject ui;
    private bool opened = false;
    private Animator uiAnim;

    private AudioSource audioSource;


    void Start() {
        ui = Player.weaponUi;
        uiAnim = ui.GetComponent<Animator>();
        audioSource = FindObjectOfType<AudioSource>();
    }

	void Awake() {
        weapons = new List<Weapon>();
        anim = gameObject.GetComponent<Animator>();
        fists = new Weapon("fists", 10, 0.5f, fistsAnim);
        dagger = new Weapon("dagger", 10, 0.3f, daggerAnim);
        gun = new Weapon("gun", 25, 0.3f, gunAnim, gunAmmo);
        bow = new Weapon("bow", 50, 1, bowAnim, bowAmmo);

        weapons.Add(fists);
        weapons.Add(dagger);
        weapons.Add(gun);
        weapons.Add(bow);

        setWeapon(fists, false);

	}
	
	void Update(){
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            setWeapon(fists, true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && dagger.getOwned()) {
            setWeapon(dagger, true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && gun.getOwned()) {
            setWeapon(gun, true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && bow.getOwned())
        {
            setWeapon(bow, true);
        }

        if (Input.GetButtonDown ("Fire1") && !attacking) {
			attacking = true;
			attactTimer = attackCd;

            AudioClip attackSound = (AudioClip)Resources.Load("SoundEffects/" + currentWeapon.getName());
            audioSource.PlayOneShot(attackSound);
			attackTrigger.SetActive(true);
		}
		
		if (attacking) {
			if(attactTimer > 0){
				attactTimer -= Time.deltaTime;
			}else{
				attacking = false;
				attackTrigger.SetActive(false);
			}
		}
		anim.SetBool ("Attack", attacking);
	}
	
	public bool isAttacking(){
		return attacking;
	}

    private void setWeapon(Weapon weapon, bool displayUI) {
        damage = weapon.getDamage();
        anim.runtimeAnimatorController = weapon.getAnimator();
        currentWeapon = weapon;
        attackCd = weapon.getCooldown();

        if (displayUI)
        {
            ui.SetActive(true);

            //Highlights the picture of the weapon in the ui
            foreach (Image img in ui.GetComponentsInChildren<Image>())
            {
                if (img.name.ToLower().Equals(weapon.getName()))
                {
                    img.color = new Color(255, 255, 255, 255);
                }
                else if (!img.name.Equals("back"))
                { //Prevents rest of UI from turning black
                    img.color = new Color(0, 0, 0, 179);
                }

            }
            if (!opened)
            {
                opened = true;
                StartCoroutine(wait());
            }


        }
        
    }

    public void setWeapon(string weaponName) {
        print("Weapons: " + weapons.Count);
        foreach(Weapon weapon in weapons) {
            if (weapon.getName().Equals(weaponName)) {
                setWeapon(weapon, false);
            }
        }
    }

    public Weapon findWeapon(string weaponName)
    {
        foreach (Weapon weapon in weapons)
        {
            if (weapon.getName().Equals(weaponName))
            {
                return weapon;
            }
        }
        return null;
    }

    public Weapon getWeapon() {
        return currentWeapon;
    }

    public int getDamage() {
        return damage;
    }

    public List<Weapon> getWeaponList() {
        return weapons;
    }

    private IEnumerator wait()
    {
        yield return new WaitForSeconds(1);
        uiAnim.SetTrigger("disabled");
        yield return new WaitForSeconds(1);
        ui.SetActive(false);
        opened = false;
    }
}
