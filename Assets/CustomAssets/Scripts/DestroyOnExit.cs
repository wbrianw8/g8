﻿using UnityEngine;
using System.Collections;

public class DestroyOnExit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerExit2D(Collider2D col) {
        if (col.CompareTag("Player")) {
            Destroy(gameObject);
        }
    }
}

