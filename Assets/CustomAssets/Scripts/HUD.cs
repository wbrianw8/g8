﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public Image healthBar;
    public Text healthNumber;
    public Player player;

	// Use this for initialization
	void Start () {
        if (player == null)
        {
            player = GameObject.FindObjectOfType<Player>();
        }
	}

    // Update is called once per frame
    void Update()
    {
        float currentHealth = player.currentHealth;
        healthNumber.text = currentHealth.ToString();
        healthBar.transform.localScale = new Vector3(currentHealth/100, 1);
    }
}
