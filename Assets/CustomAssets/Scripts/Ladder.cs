﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour {


    private Player player;
    private Animator anim;
    private Rigidbody2D playerbody;
    private BoxCollider2D ladderTrans;
    private bool onLadder = false;
    private float climbSpeed = 1.5f;
    private string climb;
    private string idle;
    public ladderType climbType;


    public enum ladderType {
        rope,ladder
    }

	// Use this for initialization
	void Start () {
        ladderTrans = this.GetComponent<BoxCollider2D>();
        if (climbType == ladderType.ladder)
        {
            climb = "Climbing";
            idle = "LadderIdle";
        }
        else if (climbType == ladderType.rope) {
            climb = "RopeClimb";
            idle = "RopeIdle";
        }
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
            if (Input.GetAxis("Vertical") > 0)
            {
                climbLadder(other, true);
            }
            else if (Input.GetAxis("Vertical") < 0)
            {
                climbLadder(other, false);
            }

            }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Jump"))
            {
                player.setLadder(false);
                playerbody.gravityScale = 1;
                onLadder = false;
                playerbody.AddForce(new Vector2(Input.GetAxis("Horizontal") * player.getDirection() * 200, 50));
                anim.SetBool(idle, false);
                anim.SetBool(climb, false);
            }

                if (Input.GetAxis("Vertical") > 0)
                    climbLadder(other, true);
                else if (Input.GetAxis("Vertical") < 0)
                {
                    climbLadder(other, false);
                }
                else if (Input.GetAxis("Vertical") == 0 && onLadder)
                {
                    Transform playerTrans = player.GetComponent<Transform>();
                    playerTrans.position = new Vector2(ladderTrans.bounds.center.x, playerTrans.position.y);
                    anim.SetBool(climb, false);
                    anim.SetBool(idle, true);
                    playerbody.velocity = new Vector2(0, 0);
                }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
		if (other.CompareTag("Player") && onLadder)
        {
            onLadder = false;
            anim.SetBool(idle, false);
            anim.SetBool(climb, false);
            playerbody = other.GetComponent<Rigidbody2D>();
            playerbody.velocity = new Vector2(0, 0);
            player.setLadder(false);

            playerbody.gravityScale = 1;
        }
    }

    public void climbLadder(Collider2D other, bool up) {
        onLadder = true;
        if (other.CompareTag("Player") && player == null)
        {
            player = other.GetComponent<Player>();
            anim = player.GetComponent<Animator>();
            playerbody = player.GetComponent<Rigidbody2D>();
        }
        if (!anim.GetBool("Ground"))
        {
            player.setHeight();
            anim.SetBool(climb, true);
            anim.SetBool(idle, false);
            player.setLadder(true);
            Transform playerTrans = player.GetComponent<Transform>();
            playerTrans.position = new Vector2(ladderTrans.bounds.center.x, playerTrans.position.y);
            if (up)
            {
                playerbody.velocity = new Vector2(0, climbSpeed);
            }
            else {
                playerbody.velocity = new Vector2(0, -1 * climbSpeed);
            }
            playerbody.gravityScale = 0;
        }
        else {
            OnTriggerExit2D(other);
        }
        
    }

}
