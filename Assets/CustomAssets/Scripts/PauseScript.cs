﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour {

    public GameObject PauseUi;

    private bool paused = false;
    private PlayerAttack attack;
    private bool resumed = false;

    void Start() {
        GameObject dialouge = GameObject.FindGameObjectWithTag("Dialogue");
        if (dialouge != null)
        {
            dialouge.SetActive(false);
        }
        PauseUi.SetActive(false);

        attack = FindObjectOfType<PlayerAttack>();
    }

    void Update() {
        if (Input.GetButtonDown("Pause") || resumed) {

            paused = !paused;
            if (resumed == true) paused = false;

            if (paused)
            {
                PauseUi.SetActive(true);
                attack.enabled = false;
                Time.timeScale = 0;
            }
            else {
                PauseUi.SetActive(false);
                if(attack != null)
                    attack.enabled = true;
                Time.timeScale = 1;
                resumed = false;
            }
        }



    }

    public void Save() {
        FindObjectOfType<GameManager>().save();
        resumed = true;
    }

    public void Resume() {
        resumed = true;
    }

    public void Restart() { 
        FindObjectOfType<GameManager>().loadGame();
        resumed = true;
    }

    public void MainMenu() {
        SceneManager.LoadScene(0);
    }

    public void Quit() {
        Application.Quit();
    }
}
