﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Cutscene2 : MonoBehaviour {


    // Use this for initialization
    void Start () {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Dialogue>().talk("I think its about time you join the combat inititive", "Mysterious White Guy", 4);
        StartCoroutine(wait(5));

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator wait(int time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
