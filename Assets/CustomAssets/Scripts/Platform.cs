﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {


    public bool moving;
    public float range = 10;
    public float speedPercent;
    private Rigidbody2D mRigidbody;
    private Transform trans;
    private float startingPos;
    private int direction = 1;
    public bool enableOnEnter = false;
    public GameObject rope = null;

    private Player playerScript = null;
    private Rigidbody2D player;


    private float maxSpeed;

	// Use this for initialization
	void Start () {
        trans = GetComponent<Transform>();
        mRigidbody = GetComponent<Rigidbody2D>();

        playerScript = FindObjectOfType<Player>();
        player = playerScript.GetComponent<Rigidbody2D>();
        playerScript = playerScript.GetComponent<Player>();

        maxSpeed = playerScript.maxSpeed;

        startingPos = trans.position.x;
        if (moving)
        {
            mRigidbody.velocity = new Vector2(speedPercent, 0);
        }

    }
	
	// Update is called once per frame
	void Update () {

        if (moving) {


            if (startingPos + range <= trans.position.x)
            {

                direction = -1;
            }
            else if(trans.position.x <= startingPos)
            {

                direction = 1;
            }

            mRigidbody.MovePosition((mRigidbody.position + new Vector2(speedPercent, 0) * Time.fixedDeltaTime * direction));

            if (playerScript != null && player.IsTouching(GetComponent<EdgeCollider2D>()))
            {
                player.transform.parent = transform;

            }
        }

        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        handlePlayer(other);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        handlePlayer(other);   

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (playerScript == null) playerScript = other.GetComponent<Player>();
            if (player == null) player = playerScript.GetComponent<Rigidbody2D>();
            player.transform.parent = null;
            if (moving)
            {
                playerScript.setMaxSpeed(-1);
            }
            playerScript.setOnPlatform(false);
            gameObject.GetComponent<EdgeCollider2D>().enabled = true;
            if(moving)
                playerScript = null;
        }
    }

    void handlePlayer(Collider2D other) {
        if (other.CompareTag("Player")){
            if (enableOnEnter && mRigidbody.velocity.x == 0) {
                moving = true;
                mRigidbody.velocity = new Vector2(speedPercent, 0);
            }

            if (playerScript == null) playerScript = other.GetComponent<Player>();

            if (moving && ((playerScript.facingRight && mRigidbody.velocity.x > 0) || (!playerScript.facingRight && mRigidbody.velocity.x > 0)))
            {
                playerScript.setMaxSpeed(maxSpeed * 1.5f);
            }
            else {
                playerScript.setMaxSpeed(-1);
            }

                playerScript.setOnPlatform(true);


            if (playerScript.isCrouched() && Input.GetButton("Jump"))
            {
                gameObject.GetComponent<EdgeCollider2D>().enabled = false;
                playerScript.setOnPlatform(false);
            }
            

            if (rope != null)
            {
                if (rope.GetComponent<BoxCollider2D>().IsTouching(other.GetComponent<BoxCollider2D>()) && Input.GetAxis("Vertical") < 0)
                {
                    gameObject.GetComponent<EdgeCollider2D>().enabled = false;
                    rope.GetComponent<Ladder>().climbLadder(other, false);
                    playerScript.setOnPlatform(false);
                }
            }
        }
    }
}
