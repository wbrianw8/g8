﻿using UnityEngine;
using System.Collections;

public class MultiAttackEnemy : EnemyMelee {

    private float attack2Timer;
    private float attack3Timer;

    //Attack 1 should be the melee attack
    public float attack2Cd;
    public float attack3Cd;

    private BoxCollider2D attack2Collider;
    private BoxCollider2D attack3Collider;
    private Animator anim;

    // Use this for initialization
    new void Start () {
        base.Start();
        attack2Timer = attack2Cd;
        attack3Timer = attack3Cd;
        anim = GetComponent<Animator>();

        //Enemy should have two children for ranged attacks
        foreach (BoxCollider2D collider in GetComponentsInChildren<BoxCollider2D>()) {
            if (collider.name.Equals("attack2")) attack2Collider = collider;
            if (collider.name.Equals("attack3")) attack3Collider = collider;
        }
	}
	
	// Update is called once per frame
	new void Update () {
        base.Update();

        //Reduces cooldowns
        attack2Timer -= Time.deltaTime;
        attack3Timer -= Time.deltaTime;

        if (curHealth <= 0)
        {
            death();
        }

    }

    //Prevents animations from stopping death
    void death() {
        anim.SetBool("Dying", true);
        attack2Timer = 20;
        attack3Timer = 20;
    }

    public void attackColliderEntered(BoxCollider2D triggerEntered) {
        if (triggerEntered.Equals(attack2Collider) && attack2Timer <= 0) {

            //Stops the enemy from moving and so that other animations don't interupt the attack
            attackTimer = 2f;
            if (attack3Timer <= 0) attack3Timer = 2f;

            //Activates the enemy animation
            anim.SetTrigger("Attack2");           
            attack2Timer = attack2Cd;

            //Spawns in the animation for the attack
            Instantiate(Resources.Load("Prefabs/PirateBossAttack2"), transform.position, Quaternion.identity);
        }

        if (triggerEntered.Equals(attack3Collider) && attack3Timer <= 0)          
        {
            //Stops the enemy from moving and so that other animations don't interupt the attack
            attackTimer = 3f;
            if (attack2Timer <= 0) attack2Timer = 3f;

            //Activates the animation
            anim.SetTrigger("Attack3");
            attack3Timer = attack3Cd;

            //Spawns in the animation for the attack
            Instantiate(Resources.Load("Prefabs/PirateBossAttack3"), new Vector2(triggerEntered.transform.position.x, transform.position.y), Quaternion.identity);
        }
    }

    private void removeBarrels() {
        Destroy(GameObject.Find("Barrels"));
    }

    
}
