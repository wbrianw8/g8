﻿using UnityEngine;
using System.Collections;


//This class is put on the multiple attack colliders to notify the parent script of when it is entered.
public class MultiAttack : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player"))
        {
            GetComponentInParent<MultiAttackEnemy>().attackColliderEntered(GetComponent<BoxCollider2D>());
        }
    }

    void remove() {
        Destroy(gameObject);
    }
}
