﻿using UnityEngine;
using System.Collections;

public class Charger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other) {
        charge(other);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        charge(other);
    }

    void charge(Collider2D other) {
        if (other.CompareTag("Player")) {
            Player player = other.GetComponent<Player>();
            player.currentHealth += 1;
        }
    }
}
