﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    private Animator animator;
    private Rigidbody2D rigidBody;
    public bool facingRight;
    private bool grounded;
    [SerializeField] public float maxSpeed = 10f;
    [SerializeField] private float jumpForce = 100f;
    private float defaultSpeed;
    //private Transform groundCheck;
    //private float groundedRadius = 0.2f;
    //[SerializeField]private LayerMask whatIsGround;
    private PlayerAttack attackState;
    private BoxCollider2D playerCollider;
    private bool onLadder = false;
    public static GameObject dialogue;
    public static GameObject weaponUi;
    public static GameObject deathUI;

    private bool crouched;

    private bool onPlatform = false;
    private BoxCollider2D ropeCollider;
    private EdgeCollider2D platform;

    private List<inventoryItem> items;

    //Health System
    public int currentHealth;
    public int maxHealth = 100;

    //Fall Damage
    private float playerHeight;
    private bool ignoreFall = false;

    private GameManager gameManager;

    // Use this for initialization
    void Awake () {
        FindObjectOfType<Camera2DFollow>().setTarget(transform);

        gameManager = FindObjectOfType<GameManager>(); 

        defaultSpeed = maxSpeed;
        if (Player.dialogue == null)
        {
            dialogue = GameObject.FindGameObjectWithTag("Dialogue");
            dialogue.SetActive(false);
        }
        if (Player.weaponUi == null)
        {
            weaponUi = GameObject.FindGameObjectWithTag("WeaponUI");
            weaponUi.SetActive(false);
        }
        if (Player.deathUI == null) {
            deathUI = GameObject.FindGameObjectWithTag("DeathUI");
            deathUI.SetActive(false);
        }

        if (items == null) items = new List<inventoryItem>();

        animator = this.GetComponent<Animator>();
        rigidBody = this.GetComponent<Rigidbody2D>();
        facingRight = animator.GetBool("FacingRight");
        attackState = this.GetComponent<PlayerAttack>();
        playerCollider = this.GetComponent<BoxCollider2D>();

        playerHeight = transform.position.y;

        currentHealth = maxHealth;
        Flip();

    }

    void Update() {
        if (currentHealth > maxHealth) currentHealth = maxHealth;
        if (currentHealth <= 0) die();
    }
	// Update is called once per frame
	void FixedUpdate () {

        grounded = isGrounded();

        animator.SetFloat("Speed", 1);
        animator.SetBool("Ground", grounded);

    }

    public void Move(float move, bool jump, float vertical)
    {
        if (vertical < 0 && grounded && !onLadder)
        {
            rigidBody.velocity = new Vector2(0,0);
            crouched = true;
            animator.SetBool("Crouch", true);            
        }
        else {
			
            animator.SetBool("Crouch", false);
            crouched = false;
        }

        //only control the player if grounded or airControl is turned on
        if (!onLadder && !crouched)
        {
            if (move != 0)
            {
                animator.SetInteger("Direction", 1);
            }
            else
            {
				
                animator.SetInteger("Direction", 0);
            }
            // The Speed animator parameter is set to the absolute value of the horizontal input.
			
            animator.SetFloat("Speed", Mathf.Abs(move));

            // Move the character
            if (onPlatform && move == 0)
            {

            }
            else {
                
                rigidBody.velocity = new Vector2(move * maxSpeed, rigidBody.velocity.y);
            }


            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !facingRight)
            {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && facingRight)
            {
                // ... flip the player.
                Flip();
            }


        }

        // If the player should jump...
        if (grounded && jump && animator.GetBool("Ground") && !crouched)
        {
            // Add a vertical force to the player.
            if (grounded)
            {
                rigidBody.AddForce(new Vector2(0f, jumpForce));
            }
        }
    }
    public void setMaxSpeed(float change) {
        if (change < 0)
        {
            maxSpeed = defaultSpeed;
        }
        else {
            maxSpeed = change;
        }
        
    }

    public void setCrouch(bool state) {
        crouched = state;
    }

    public bool isCrouched() {
        return crouched;
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    //Restarts level
    void die() {
		gameObject.GetComponent<BoxCollider2D> ().enabled = false;
		gameObject.GetComponent<SpriteRenderer> ().enabled = false;
        rigidBody.isKinematic = false;
        rigidBody.gravityScale = 0;
        rigidBody.velocity = new Vector2(0, 0);
		deathUI.SetActive(true);
        Destroy(gameObject);
    }


    public void applyDamage(int damage) {
        if (gameManager == null) gameManager = FindObjectOfType<GameManager>();
        AudioClip sound = (AudioClip)Resources.Load("SoundEffects/damaged");
        gameManager.getSoundEffects().PlayOneShot(sound);

        if (currentHealth - damage < 0)
        {
            currentHealth = 0;
        }
        else {
            currentHealth -= damage;
        }
    }

    public int getDirection() {
        if (facingRight)
        {
            return -1;
        }
        else {
            return 1;
        }
    }

    public void knockback(float enemyPos) {
        if (!animator.GetBool("Attack"))
        {
            if (playerCollider.transform.position.x > enemyPos)
            {
                rigidBody.transform.Translate(new Vector3(1, (float)0.5).normalized);
               // rigidBody.velocity = new Vector2(10, 1);
            }
            else
            {
                rigidBody.transform.Translate(new Vector3(-1, (float)0.5).normalized);
                // rigidBody.velocity = new Vector2(-10, 1);
            }
        }
    }


    private bool isGrounded() {
        
        Vector2 height = new Vector2(rigidBody.position.x + playerCollider.bounds.extents.x + (float)0.0001, rigidBody.position.y - playerCollider.bounds.extents.y);
        Vector2 height2 = new Vector2(rigidBody.position.x - playerCollider.bounds.extents.x, rigidBody.position.y - playerCollider.bounds.extents.y);
        Debug.DrawRay(height, -Vector2.up, Color.white);
        Debug.DrawRay(height2, -Vector2.up, Color.white);
        RaycastHit2D cast =  Physics2D.Raycast(height, -Vector2.up, 0.05f);
        RaycastHit2D cast2 = Physics2D.Raycast(height2, -Vector2.up, 0.05f);
        if (cast.collider == null && cast2.collider == null)
        {
            return false;
        }
        if (cast.collider == null)
        {
            if (!cast2.collider.CompareTag("Floor"))
            {
                return false;
            }
        }
        else
        {
            if (!cast.collider.CompareTag("Floor"))
            {
                return false;
            }
        }

        //fall damage
        float newHeight = transform.position.y;
        if (playerHeight - newHeight > 3 && !ignoreFall) {
			//testing exponential fall damage
			applyDamage((int)Mathf.Pow(10,(playerHeight - newHeight)/5));
            ignoreFall = false;
        }
        playerHeight = newHeight;
        return true;
    }

    public void setHealth(int amount) {
        currentHealth = amount;
    }

    public void pickup(Pickup item) {
        if (item.getType().Equals("Weapon"))
        {
            attackState.findWeapon(item.getName()).setOwned(true);
        }
        else if (item.getType().Equals("Health"))
        {
            currentHealth += 20;
        }
        else if (item.getType().Equals("Item")) {
            bool itemAdded = false;
            if (items == null) items = new List<inventoryItem>();
            foreach(inventoryItem thing in items){
                if (thing.getName().Equals(item.getName())) {
                    thing.add();
                    itemAdded = true;
                    break;
                }
            }
            if(!itemAdded) items.Add(new inventoryItem(item.getName()));
            foreach (inventoryItem thing in items) print(thing.getName());
        }
    }

    public List<inventoryItem> getItems() {
        if (items == null) items = new List<inventoryItem>();
        return items;
    }

    public int amountOf(string item) {
        foreach (inventoryItem thing in items) {
            if (thing.getName().Equals(item))
            {
                return thing.getAmount();
            }
        }
        return 0;
    }

    public void removeItem(string name, int amount) {
        foreach (inventoryItem item in items) {
            if (item.getName().Equals(name)) {
                item.remove(amount);
            }
        }
    }

    public void setItems(List<inventoryItem> inv) {
        items = inv;
    }

    public void setLadder(bool state) {
        onLadder = state;
    }

    public bool getLadder() {
        return onLadder;
    }

    public void setOnPlatform(bool state) {
        onPlatform = state;
    }

    public bool isOnPlatform() {
        return onPlatform;
    }


    public void setHeight() {
        playerHeight = transform.position.y;
    }

}
