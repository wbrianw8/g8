﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Player))]
public class PlayerController : MonoBehaviour {

    private Player player;
    private bool hasJumped;

    private void Update() {
        hasJumped = Input.GetButtonDown("Jump");
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        player.Move(horizontal, hasJumped, vertical);
        hasJumped = false;
    }
    // Use this for initialization
    private void Awake() {
        player = GetComponent<Player>();
    }

    // Update is called once per frame
    private void FixedUpdate() {

        
    }


    
}


