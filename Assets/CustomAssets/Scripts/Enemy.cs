﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Enemy : MonoBehaviour {

    public abstract void setHealth(int amount);

    public abstract int getHealth();

    //Name must be the same as the name of the prefab
    public abstract string getName();

    public abstract void setName(string name);

	public abstract void Damage(int damage);

    public abstract int getMaxHealth();
}
