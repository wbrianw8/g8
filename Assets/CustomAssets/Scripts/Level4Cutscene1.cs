﻿using UnityEngine;
using System.Collections;

public class Level4Cutscene1 : MonoBehaviour {

    public GameObject indian;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col) {
        if (col.CompareTag("Player")) {
            indian.GetComponent<SpriteRenderer>().sortingOrder = 1;
            indian.GetComponent<Dialogue>().talk("Me Great Big Ocean, who you?", "Great Big Ocean", 2);
            indian.GetComponent<Follower>().enabled = true;
            Destroy(gameObject);
        }
    }
}
