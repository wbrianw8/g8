﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    private Player player;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnEnable()
    {
        foreach (Transform child in GetComponentsInChildren<Transform>()) {
            if (!child.name.Equals("Content"))
                Destroy(child.gameObject);
        }
        if (player == null) {
            player = FindObjectOfType<Player>();
        }

        List<inventoryItem> items = player.getItems();

        if (items != null)
        {
            for (int i = 0; i < items.Count; i++)
            {
                GameObject created = (GameObject)Instantiate(Resources.Load("Prefabs/Item"), new Vector2(0,0), Quaternion.identity);
                RectTransform childBox = created.GetComponent<RectTransform>();
                created.transform.SetParent(gameObject.transform);
                childBox.position = new Vector2(transform.position.x, gameObject.transform.position.y - created.GetComponent<RectTransform>().rect.height * (items.Count - i - 1) - 25);
                childBox.anchoredPosition = new Vector2(0, childBox.anchoredPosition.y);
                childBox.localScale = new Vector2(1, 1);

                Image itemImage = created.GetComponentInChildren<Image>();
                itemImage.sprite = Resources.Load<Sprite>("Items/" + items[i].getName());
                foreach (Text field in created.GetComponentsInChildren<Text>())
                {
                    if (field.name.Equals("Amount")) field.text = items[i].getAmount() + "";
                    else {
                        string name = items[i].getName();
                        name = name.Substring(0, 1).ToUpper() + name.Substring(1);
                        field.text = name;
                    }
                }
            }

        }
    }
}
