﻿using UnityEngine;
using System.Collections;

public class nonmovingProjectile : MonoBehaviour {

    public int damage = 0;
    public bool destroyOnHit = true;
    public bool continuousDamage = false;

    private float damageTimer;
    public float continuousPauseDuration;

    private bool damaged = false;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        damageTimer -= Time.deltaTime;
	}

    public void setDamage(int amount) {
        damage = amount;
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.CompareTag("Player")) {
            if(!damaged) //Prevents user from being hit multiple times if the full animation displays
                col.GetComponent<Player>().applyDamage(damage);
            if(destroyOnHit)
                destroy();
            damaged = true;
        }
    }

    void OnTriggerStay2D(Collider2D col) {
        if (col.CompareTag("Player"))
        {
            if (continuousDamage)
            {
                if (damageTimer <= 0)
                {
                    col.GetComponent<Player>().applyDamage(damage);
                    damageTimer = continuousPauseDuration;
                }
            }
        }
    }

    void destroy() {
        Destroy(gameObject);
    }
}
