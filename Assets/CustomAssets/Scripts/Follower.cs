﻿using UnityEngine;
using System.Collections;

public class Follower : MonoBehaviour {

    Player player;
    BoxCollider2D playerBox;
    Animator anim;
    bool facingRight = false;
    BoxCollider2D trigger;

	// Use this for initialization
	void Start () {
        anim = this.GetComponent<Animator>();
        player = GameObject.FindObjectOfType<Player>();
        playerBox = player.GetComponent<BoxCollider2D>();
        foreach(BoxCollider2D box in this.GetComponents<BoxCollider2D>()){
            if (box.isTrigger)
                trigger = box;
            else
                Physics2D.IgnoreCollision(box, player.GetComponent<BoxCollider2D>());
        }
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        int direction = facingRight ? -1 : 1;
        float playerX = player.transform.position.x + (playerBox.bounds.extents.x * 2 + 0.2f) * direction;
        if (playerX != transform.position.x && !trigger.IsTouching(playerBox))
        {
            anim.SetBool("Walking", true);

            if (transform.position.x < player.transform.position.x - 0.3 && player.facingRight != facingRight)
            {
                    Flip();
            }
            else if (transform.position.x > player.transform.position.x + 0.3 && player.facingRight != facingRight) Flip();
            else {
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(playerX, transform.position.y), Time.deltaTime * 1.9f);
            }


            
        }
        else {
            anim.SetBool("Walking", false);
        }
	}

    private void Flip()
    {
        // Multiply the player's x local scale by -1.
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
