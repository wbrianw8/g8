﻿using UnityEngine;
using System.Collections;

public class FlingyThingy : MonoBehaviour {

    public bool playerOnly = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") || !playerOnly)
        {
            Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0, 7);
        }
    }
}
