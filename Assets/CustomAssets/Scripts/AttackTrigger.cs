﻿using UnityEngine;
using System.Collections;

public class AttackTrigger : MonoBehaviour {

	public int dmg = 20;
    public GameObject projectile;

    void OnEnable() {
        PlayerAttack attack = FindObjectOfType<PlayerAttack>();
        dmg = attack.getDamage();

        Weapon curWeapon = attack.getWeapon();
        if (curWeapon != null && curWeapon.isRanged()) {
            projectile.GetComponent<SpriteRenderer>().sprite = curWeapon.getAmmo();
            Player player = FindObjectOfType<Player>();
            Vector2 scale = transform.localScale;
            scale.x *= player.getDirection();
            transform.localScale = scale;
            Vector2 startingLocation = player.transform.position;
            startingLocation.x -= player.GetComponent<BoxCollider2D>().bounds.size.x * player.getDirection();
            startingLocation.y -= player.GetComponent<BoxCollider2D>().bounds.size.y/16;

            Instantiate(projectile, startingLocation , Quaternion.identity);
        }
    }

	void OnTriggerEnter2D(Collider2D col){
		if(col.isTrigger != true && col.CompareTag("Enemy")){
			col.SendMessageUpwards("Damage", dmg);
		}
	}
}
