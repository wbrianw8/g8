﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour {

    public GameObject target;
    private float yOffset;
    private Enemy enemy;
    private Image background;
    private Image healthBar;

	// Use this for initialization
	void Start () {
        transform.SetParent(FindObjectOfType<Canvas>().transform);
        foreach (Image image in GetComponentsInChildren<Image>()) {
            if (image.name.Contains("Healthbar")) background = image;
            else healthBar = image;
        }
        background.enabled = false;
        healthBar.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(enemy != null && enemy.getHealth() < enemy.getMaxHealth())
        {
            healthBar.transform.localScale = new Vector3(((float)enemy.getHealth()) / enemy.getMaxHealth(), 1);
            if(enemy.getHealth() < 0) healthBar.transform.localScale = new Vector3(0, 1);
            background.enabled = true;
            healthBar.enabled = true;
            Vector2 adjustedPos = new Vector2(target.transform.position.x, target.transform.position.y + yOffset);
            Vector2 wantedPos = Camera.main.WorldToScreenPoint(adjustedPos);
            transform.position = wantedPos;
        }
        
    }

    public void setTarget(GameObject tar)
    {
        target = tar;
        enemy = target.GetComponent<Enemy>();
        yOffset = target.GetComponent<BoxCollider2D>().bounds.extents.y;
    }
}
