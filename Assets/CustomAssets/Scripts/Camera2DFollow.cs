using System;
using UnityEngine;

    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
        public float yPosition;
        public GameObject maxLeft;
        public GameObject maxRight;
        private float cameraSize;

        private Camera mCamera;
        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;
        private float minumumX;
        private float maximumX;

        // Use this for initialization
        private void Start()
        {
            mCamera = Camera.main;
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
            cameraSize = mCamera.orthographicSize * mCamera.aspect;
			minumumX = maxLeft.GetComponent<BoxCollider2D> ().transform.position.x + maxLeft.GetComponent<BoxCollider2D>().bounds.size.x / 2;
			maximumX = maxRight.GetComponent<BoxCollider2D>().transform.position.x - maxRight.GetComponent<BoxCollider2D>().bounds.size.x / 2;
			yPosition = maxRight.GetComponent<BoxCollider2D> ().transform.position.y - maxRight.GetComponent<BoxCollider2D> ().bounds.size.y / 2 + mCamera.orthographicSize;
        }


        // Update is called once per frame
        private void Update()
        {
            if (target == null) target = transform;
            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
            if (aheadTargetPos.x + cameraSize >= maximumX ){
                aheadTargetPos.x = maximumX - cameraSize;
            }
            if (aheadTargetPos.x - cameraSize <= minumumX) {
                aheadTargetPos.x = minumumX + cameraSize;
            }
            if (aheadTargetPos.y < yPosition) {
                aheadTargetPos.y = yPosition;
            }
            aheadTargetPos = new Vector3(aheadTargetPos.x, aheadTargetPos.y, m_OffsetZ);
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

            transform.position = newPos;

            m_LastTargetPosition = target.position;
        }
        public void setTarget(Transform player)
        {
            target = player;
        }
    }

