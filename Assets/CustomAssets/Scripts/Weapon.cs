﻿using UnityEngine;
using System.Collections;

public class Weapon{

    public RuntimeAnimatorController animator;
    private string weaponName;
    private int damage;
    private Sprite ammo;
    private bool ranged = false;
    private float cooldown;
    private bool owned;


    public Weapon(string name, int damage, float attackCooldown, RuntimeAnimatorController anim) {
        System.Console.WriteLine(name + " created.");
        weaponName = name;
        this.damage = damage;
        animator = anim;
        owned = false;
        cooldown = attackCooldown;
    }

    public Weapon(string name, int damage, float attackCooldown, RuntimeAnimatorController anim, Sprite ammo)
    {
        weaponName = name;
        this.damage = damage;
        animator = anim;
        this.ammo = ammo;
        ranged = true;
        owned = false;
        cooldown = attackCooldown;
    }

    public string getName() {
        return weaponName;
    }

    public int getDamage() {
        return damage;
    }

    public RuntimeAnimatorController getAnimator() {
        return animator;
    }

    public bool getOwned() {
        return owned;
    }

    public void setOwned(bool value) {
        owned = value;
    }

    public Sprite getAmmo() {
        return ammo;
    }

    public bool isRanged() {
        return ranged;
    }

    public float getCooldown() {
        return cooldown;
    }
}
