using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class EnemyMelee : Enemy, IDamagable {
	
	public int damage = 10;
	public float speed;

	public LayerMask walls;
	
	public float attackTimer = 0;
	public float attackCd = 0.5f;
	public int attacksHit = 0;
    public bool accurateHit = false; //If true you MUST specifiy in the animation when to apply damage

    public bool moving = true;
    public bool faceLeft = false;
	
	public int maxHealth = 100;
    public int curHealth;
    public string enemyName; //Set to the name of the enemy prefab

    public Player player;

    public int dropPercentage;
    public GameObject dropItem; //Assign a prefab that you want the enemy to drop

    protected bool dead = false;
    private GameObject healthBar;

    private List<string> avoid;
    private int direction;
    private bool facingRight;
    private Rigidbody2D rigidBody;
    protected BoxCollider2D enemyCollider;
    private Animator animator;

    // Use this for initialization
    protected void Start() {

        healthBar = (GameObject)Instantiate(Resources.Load("Prefabs/Healthbar"));
        healthBar.GetComponent<EnemyHealthBar>().setTarget(gameObject);

        //Things enemy will not turn around for
        avoid = new List<string>();
        avoid.Add("RightDoor");
        avoid.Add("LeftDoor");
        avoid.Add("Followers");
        avoid.Add("Enemy");
        avoid.Add("Player");

        //If the enemy isn't spawned in set its health to max
        if (curHealth == 0) {
            curHealth = maxHealth;
        }

        player = FindObjectOfType<Player>();
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        foreach (BoxCollider2D collider in GetComponents<BoxCollider2D>()) {
            if(!collider.isTrigger) enemyCollider = collider;

        }

        direction = -1;

        //Checkbox facingLeft control which flips the sprite so it faces right
        if(!faceLeft)
            Flip();
        
		
	}
	
	protected void Update() {
		
        //Moving checkbox controls if the enemy walks
        if(moving)
		    walk();

		attack();

        if (curHealth <= 0 && !dead) {
            die();
		}
		
	}

    public void die() {
        //Prevents the player from colliding with the enemy as it goes through its death animation
        Physics2D.IgnoreCollision(enemyCollider, player.GetComponent<BoxCollider2D>());

        //Prevents enemy from playing attack annimation
        attackTimer = 5;
        animator.SetBool("Dead", true);
        dead = true;
        
    }

    //This method is used on the death animation to remove the enemy from the scene
    void remove() {
        drop();

        GetComponent<SpriteRenderer>().enabled = false;
        Destroy(healthBar);
        Destroy(gameObject);
    }
	
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if (hitWall())
		{
			Flip();
		}
	}
	
	void walk() {
		if (attackTimer <= 0)
		{
			animator.SetBool("Walking", true);
			rigidBody.velocity = new Vector2(direction * speed, rigidBody.velocity.y);
		}
		else {
			rigidBody.velocity = new Vector2(0, 0);
			animator.SetBool("Walking", false);
		}
	}
	
    //Draws a raycast infront of the enemy to detect if it is close to a wall
	bool hitWall() {
        //Adjusts the start position so it doesn't hit the enemy
        Vector2 castStart = new Vector2(transform.position.x + (enemyCollider.size.x * Math.Abs(transform.localScale.x) / 2 + .1f) * direction, transform.position.y);
        Debug.DrawRay(castStart, Vector2.right * direction, Color.white);
        RaycastHit2D wallCast = Physics2D.Raycast(castStart, Vector2.right * direction, 0.1f, LayerMask.GetMask("Walls", "EnemyBlock"));

        //Sees if the cast hit anything
        if (wallCast.collider == null) {
            return false;
        }
        if (avoid.Contains(wallCast.collider.gameObject.tag) || ( player != null && wallCast.collider.transform.IsChildOf(player.transform)))
        {
            return false;
        }

        return true;
	}
	
	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) {

            //If the enemy walks into the enemies colliders attacks them
            if (attackTimer <= 0)  //Timer delays hits so the enemies don't auto kill the player
                {
                    attackTimer = attackCd;
                    attacksHit++;
                    enemyCollider.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0); //Stops the enemy from moving
                }
            }   
    }

    void OnCollisionStay2D(Collision2D other) {

        if (other.collider.CompareTag("Player"))
        {
            if (attackTimer <= 0)
            {
                attackTimer = attackCd;
                attacksHit++;
            }
        }
    }

	
	public virtual void attack() {

		if (attacksHit > 0 && curHealth > 0) //Hits given on collision if they didn't have a cooldown
		{
            //Changes direction to face player when attacking
        if ((facingRight && player.transform.position.x < gameObject.transform.position.x) || (!facingRight && player.transform.position.x > gameObject.transform.position.x))
            {
				Flip();
			}
			stop();
            if(!accurateHit)
                player.applyDamage(damage);
            animator.SetTrigger("Attack");
			attacksHit--;
		}
		
		if (attackTimer > 0) {
			attackTimer -= Time.deltaTime;
		}
	}
	
	void stop(){
		attackTimer = attackCd;		
		rigidBody.velocity = new Vector2 (0, 0);
	}


    //Call on frame of animation when you want damage to be applied.
    void attackNow() {
        foreach (Collider2D col in GetComponents<BoxCollider2D>()) {
            if (col.IsTouching(player.GetComponent<BoxCollider2D>())) {
                player.applyDamage(damage);
            }
        }
    }

    private void Flip()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		direction *= -1;

    }
	
    //Applies damage to the enemy
	public override void Damage(int damage){
        if (!dead)
        {
            curHealth -= damage;
            animator.SetTrigger("Damaged");

            //Flips to face player if they aren't facing the same direction
            if ((facingRight && player.transform.position.x < gameObject.transform.position.x) || (!facingRight && player.transform.position.x > gameObject.transform.position.x))
            {
                attackTimer = 1f;
                Flip();
            }
        }
	}

    //Drops item that can set in the inspector with a percent chance
    public void drop()
    {
        if (dropItem != null) {
            System.Random rand = new System.Random();
            if(rand.Next(100) <= dropPercentage)
            {
                Instantiate(dropItem, transform.position, Quaternion.identity);
            }
        }
    }

    public void setAttackingTimer(int duration) {
        attackTimer = duration;
    }

    public override void setHealth(int amount)
    {
        curHealth = amount;
    }

    public override int getHealth()
    {
        return curHealth;
    }

    public override string getName()
    {
        return enemyName;
    }

    public override void setName(string name)
    {
        enemyName = name;
    }

    public override int getMaxHealth()
    {
        return maxHealth;
    }

    public bool isFacingRight() {
        return facingRight;
    }

    public void toggleFacingRight() {
        facingRight = !facingRight;
        direction *= -1;
    }
}
