﻿using UnityEngine;
using System.Collections;

public class FallDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Player player = other.GetComponent<Player>();
            if(player.currentHealth > 0)
                player.applyDamage(player.currentHealth);
        }
            
           
    }

}

