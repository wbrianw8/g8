﻿using UnityEngine;
using System.Collections;

public class RemoveOstrich : MonoBehaviour {

    //public RuntimeAnimatorController playerController;
    public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {

            Instantiate(player, other.transform.position, Quaternion.identity);
            GameObject ostrich = (GameObject) Resources.Load("Prefabs/Ostrich");
            Vector2 pos = new Vector2(other.transform.position.x, transform.position.y + ostrich.GetComponent<SpriteRenderer>().bounds.extents.y);
            ostrich.GetComponent<SpriteRenderer>().flipX = true;
            Instantiate(ostrich, pos, Quaternion.identity);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
