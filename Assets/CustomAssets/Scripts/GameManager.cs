﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager manager;
    public List<AudioClip> music;
    public List<int> musicChange;

    public int currentLevel;
    private int lastLevel;
    private int health;
    private int totalScenes;
    private LevelData[] levels;
    public Player player;
    private List<String> weapons;
    private string currentWeapon;
    private List<inventoryItem> inventory;

    private bool fromLoad;
    private bool fromDoor;
    private string colliderName = null;
    private Vector2 position;

    private AudioSource musicManager;
    private AudioSource effectsManager;
    // Use this for initialization
    void Awake () {
        health = 100;
        fromLoad = false;
        levels = new LevelData[SceneManager.sceneCountInBuildSettings];
        currentLevel = SceneManager.GetActiveScene().buildIndex;
        player = FindObjectOfType<Player>();

        if (manager == null)
        {
            DontDestroyOnLoad(this);
            manager = this;
            AudioSource[] sources = GetComponents<AudioSource>();
            musicManager = sources[0];
            effectsManager = sources[1];

            musicManager.clip = getMusic();
            musicManager.Play();
        }
        else {
            DestroyImmediate(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	}

    void setup()
    {

        FindObjectOfType<Camera>().GetComponent<Camera2DFollow>().setTarget(player.GetComponent<Transform>());
        Follower follower = FindObjectOfType<Follower>();
        if (colliderName == null || colliderName.Length < 1)
        {
            if (lastLevel < SceneManager.GetActiveScene().buildIndex)
            {
                if (lastLevel != 0)
                {
                    GameObject leftDoor = GameObject.FindGameObjectWithTag("LeftDoor");
                    if (leftDoor != null)
                    {
                        BoxCollider2D leftCollider = leftDoor.GetComponent<BoxCollider2D>();
                        player.transform.position = new Vector2(leftCollider.transform.position.x, leftCollider.transform.position.y - leftCollider.bounds.size.y);
                        player.setHeight();
                    }
                }
            }
            else
            {
                BoxCollider2D rightCollider = GameObject.FindGameObjectWithTag("RightDoor").GetComponent<BoxCollider2D>();
                player.transform.position = new Vector2(rightCollider.transform.position.x, rightCollider.transform.position.y - rightCollider.bounds.size.y);
                player.setHeight();
            }
        }
        else
        {

            print("additional doors" + colliderName);
            GameObject[] colliders = GameObject.FindGameObjectsWithTag("OtherDoor");
            foreach (GameObject collider in colliders)
            {
                if (collider.name.Equals(colliderName))
                {
                    player.transform.position = collider.transform.position;
                    break;

                }
            }
            //Puts follower next to player
            if (follower != null && !SceneManager.GetActiveScene().name.Equals("Level4.0")) follower.transform.position = new Vector2(player.transform.position.x, player.transform.position.y + follower.GetComponent<BoxCollider2D>().size.y);
        }
    }

    public void load(int lastScene, string doorName) {
        lastLevel = lastScene;
        colliderName = doorName;
    }

    //Checks the list of levels when the music changes and returns what song should be playing
    private AudioClip getMusic() {
        for (int i = 0; i < musicChange.Count - 1; i++) {
            if (currentLevel >= musicChange[i] && currentLevel < musicChange[i + 1]){
                return music[i];
            }
        }

        if (currentLevel >= musicChange[musicChange.Count - 1]) {
            return music[musicChange.Count - 1];
        }
        return music[0];
    }

    void OnLevelWasLoaded() {
        player = FindObjectOfType<Player>();
        setup();
        currentLevel = SceneManager.GetActiveScene().buildIndex;

        //Sets the music on level load
        AudioSource audio = GetComponent<AudioSource>();
        print(audio.clip.name + " " + getMusic().name);
        if (!audio.clip.Equals(getMusic()))
        {
            audio.clip = getMusic();
            audio.Play();
        }

        if (inventory != null)
        {
            player.setItems(inventory);
            player.setHealth(health);
        }
        if (currentWeapon != null)
        {
            player.GetComponent<PlayerAttack>().setWeapon(currentWeapon);
        }

        if (weapons != null)
        {
            foreach (string name in weapons)
            {
                player.GetComponent<PlayerAttack>().findWeapon(name).setOwned(true);
            }
        }

        if (fromLoad)
        {
            player.transform.position = position;
            fromLoad = false;
        }

        if (levels[currentLevel] != null)
        {
            foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) {
                DestroyImmediate(enemy);
            }

            foreach (PlayerData enemy in levels[currentLevel].enemies)
            {
                GameObject newEnemy = (GameObject)Instantiate(Resources.Load("Prefabs/" + enemy.type), new Vector2(enemy.x, enemy.y), Quaternion.identity);
                Enemy test = newEnemy.GetComponent<Enemy>();
                test.setName(enemy.type);
                if (enemy.type.Equals("Monkey")) {
                    EnemyRanged monkey = test.GetComponent<EnemyRanged>();
                    monkey.faceLeft = enemy.facingLeft;
                    monkey.moving = enemy.moving;
                }
                newEnemy.GetComponent<Enemy>().setHealth(enemy.health);
            }

        }

        if (fromDoor)
        {
            print("Saved");
            save();
            fromDoor = false;
        }
        Time.timeScale = 1;
        
    }


    public void save() {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        levels[currentLevel] = new LevelData();
        print("Enemies: " + enemies.Length);
        foreach (Enemy enemy in enemies) {
            PlayerData enemyData = new PlayerData();
            enemyData.x = enemy.transform.position.x;
            enemyData.y = enemy.transform.position.y;
            enemyData.health = enemy.getHealth();
            enemyData.type = enemy.getName();
            if (enemy.getName().Equals("Monkey")) {
                EnemyRanged ranged = enemy.GetComponent<EnemyRanged>();
                enemyData.facingLeft = ranged.faceLeft;
                enemyData.moving = ranged.moving; 
            }
            levels[currentLevel].enemies.Add(enemyData);
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream playerFile = File.Create(Application.persistentDataPath + "/playerData.dat");
        FileStream file = File.Create(Application.persistentDataPath + "/gameData.dat");

        PlayerData data = new PlayerData();
        Player player = FindObjectOfType<Player>();
        data.health = player.currentHealth;
        data.items = player.getItems();
        Vector2 location = player.transform.position;
        data.x = location.x;
        data.y = location.y;

        data.weaponName = player.GetComponent<PlayerAttack>().getWeapon().getName();
        foreach (Weapon weapon in player.GetComponent<PlayerAttack>().getWeaponList()) {
            if (weapon.getOwned()) data.weapons.Add(weapon.getName());
        }
                    
        data.currentLevel = currentLevel;
        bf.Serialize(playerFile, data);
        bf.Serialize(file, levels);
        file.Close();
        playerFile.Close();

        health = data.health;
        currentWeapon = data.weaponName;
        weapons = data.weapons;
        inventory = data.items;
    }

    public void loadGame() {
        if (File.Exists(Application.persistentDataPath + "/gameData.dat") && File.Exists(Application.persistentDataPath + "/playerData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gameData.dat", FileMode.Open);
            levels = (LevelData[])bf.Deserialize(file);
            file.Close();

            FileStream playerFile = File.Open(Application.persistentDataPath + "/playerData.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(playerFile);
            playerFile.Close();

            currentLevel = data.currentLevel;
            print("loading level");
            SceneManager.LoadScene(currentLevel);

            fromLoad = true;
            health = data.health;
            position = data.getPosition();
            weapons = data.weapons;
            currentWeapon = data.weaponName;
            inventory = data.items;
        }
        else {
            print("Couldn't find file");
        }
    }

    public AudioSource getSoundEffects() {
        if (effectsManager == null)
            effectsManager = GetComponents<AudioSource>()[1];
        return effectsManager;
    }

    public void clearLocals()
    {
        health = 100;
        fromLoad = false;
        currentWeapon = "fists";
        weapons = new List<string>();
        lastLevel = 0;
    }

    public void saveOnLoad(bool state) {
        fromDoor = state;
    }

    [Serializable]
    public class PlayerData
    {
        public int currentLevel;
        public int health;
        public float x;
        public float y;
        public string type;
        public List<string> weapons = new List<string>();
        public List<inventoryItem> items;
        public string weaponName;
        public bool moving;
        public bool facingLeft;

        public Vector2 getPosition() {
            return new Vector2(x, y);
        }
    }

    [Serializable]
    class LevelData{
        public ArrayList enemies = new ArrayList();

    }

}
