﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EyeOpening : MonoBehaviour {

    public GameObject topLid;
    public GameObject bottomLid;
    public GameObject healthBar;
    public AttackTrigger trigger;
    public GameObject guard;
    public GameObject action;

    private BoxCollider2D topCollider;
    private BoxCollider2D bottomCollider;
    private Vector2 topFirst;
    private Vector2 bottomFirst;
    private Vector2 middle;
    private Vector2 botMiddle;
    private Vector2 topFinal;
    private Vector2  botFinal;

    private bool reachedTop = false;
    private bool reachedMiddle = false;

    private Player player;
    private PlayerController control;

	// Use this for initialization
	void Start () {
        action.SetActive(false);
        guard.SetActive(false);
        trigger.enabled = false;
        topLid.SetActive(true);
        bottomLid.SetActive(true);
        player = gameObject.GetComponent<Player>();
        control = gameObject.GetComponent<PlayerController>();
        control.enabled = false;
        GetComponent<PlayerAttack>().enabled = false;
        print(GetComponent<PlayerAttack>().enabled);
        player.enabled = false;
        healthBar.SetActive(false);
        topCollider = topLid.GetComponent<BoxCollider2D>();
        bottomCollider = bottomLid.GetComponent<BoxCollider2D>();
        float height = topCollider.size.y;
        topFirst = new Vector2(topCollider.transform.position.x, topCollider.transform.position.y + height * 2);
        bottomFirst = new Vector2(bottomCollider.transform.position.x, bottomCollider.transform.position.y - bottomCollider.size.y * 2);
        middle = new Vector2(topLid.transform.position.x, topLid.transform.position.y);
        botMiddle = new Vector2(bottomLid.transform.position.x, bottomLid.transform.position.y);

        topFinal = new Vector2(topFirst.x, topFirst.y + topCollider.size.y * 2);
        botFinal = new Vector2(bottomFirst.x, bottomFirst.y - bottomCollider.size.y * 2);

    }

    // Update is called once per frame
    void Update () {
        if (topLid.transform.position.y == topFirst.y) reachedTop = true;

        if (reachedTop)
        {
            //print(topCollider.transform.position.y + "/" + middle.y);
            topLid.transform.position = Vector2.MoveTowards(topLid.transform.position, middle, .5f * Time.deltaTime);
            bottomLid.GetComponent<Transform>().position = Vector2.MoveTowards(bottomLid.transform.position, botMiddle, .5f * Time.deltaTime);
            if (Mathf.Abs(topCollider.transform.position.y - middle.y) < 0.02 ) reachedMiddle = true;

            if (reachedMiddle) {

                //print(topFinal.y + "/" + topLid.transform.position.y);
                topLid.GetComponent<Transform>().position = Vector2.MoveTowards(topLid.transform.position, topFinal, Time.deltaTime);
                bottomLid.GetComponent<Transform>().position = Vector2.MoveTowards(bottomLid.transform.position, botFinal, Time.deltaTime);

                if (topFinal.y == topLid.transform.position.y) {
                    trigger.enabled = true;
                    Destroy(topLid);
                    Destroy(bottomLid);
                    guard.SetActive(true);
                    guard.GetComponent<Cutscene1>().enabled = true;
                    Destroy(this);

                }
            }

        }
        else {
            topLid.GetComponent<Transform>().position = Vector2.MoveTowards(topLid.transform.position, topFirst, .5f * Time.deltaTime);
            bottomLid.GetComponent<Transform>().position = Vector2.MoveTowards(bottomLid.transform.position, bottomFirst, .5f * Time.deltaTime);

        }

    }
}
