﻿ using UnityEngine;
using System.Collections;

public class Cutscene1 : MonoBehaviour {


    public GameObject waypoint;
    public Player player;
    public GameObject action;
    public GameObject healthBar;

    private Vector2 location;
    private Transform charTrans;
    private Animator anim;
    private CircleCollider2D waypointColl;
    private BoxCollider2D coll;
    private GameObject door;
    private BoxCollider2D doorCollider;
    private Vector2 endpoint;

    private bool talked = false;
    private bool facingRight = false;


    // Use this for initialization
    void Start () {
        charTrans = GetComponent<Transform>();
        location = waypoint.GetComponent<Transform>().position;
        anim = GetComponent<Animator>();
        waypointColl = waypoint.GetComponent<CircleCollider2D>();
        coll = GetComponent<BoxCollider2D>();
        door = GameObject.Find("RightDoor");
        endpoint = door.GetComponent<Transform>().position;
        doorCollider = door.GetComponent<BoxCollider2D>();

    }

    // Update is called once per frame
    void Update () {
        if (!waypointColl.IsTouching(coll) && !talked)
        {
            charTrans.position = Vector2.MoveTowards(charTrans.position, location, Time.deltaTime);
            anim.SetBool("Walking", true);
        }
        else {

            if (!talked)
            {
                anim.SetBool("Walking", false);
                GetComponent<Dialogue>().talk("I see G8 is finished, he will be coming with me", "Guard", 3);
                StartCoroutine("wait", 2);
                talked = true;

            }

            if (!doorCollider.IsTouching(coll))
            {
                if (facingRight)
                {
                    anim.SetBool("Walking", true);
                    charTrans.position = Vector2.MoveTowards(charTrans.position, endpoint, Time.deltaTime);
                }
            }
            else {
                player.enabled = true;
                player.GetComponent<PlayerController>().enabled = true;
                player.GetComponent<PlayerAttack>().enabled = true;
                action.SetActive(true);
                healthBar.SetActive(true);
                Destroy(gameObject);
            }

        }
	}

    private IEnumerator wait(float time)
    {
        yield return new WaitForSeconds(time);
        if (!facingRight)
        {
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
            facingRight = true;
        }
    }

}
