﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class MainMenu : MonoBehaviour {

    public GameObject optionsMenu;
    public UnityEngine.UI.Slider volumeSlider;
    private bool optionsOpen = false;

    void Start() {
        optionsMenu.SetActive(false);
    }

    //Loads first level
    public void NewGame() {
        if (File.Exists(Application.persistentDataPath + "/gameData.dat") && File.Exists(Application.persistentDataPath + "/playerData"))
        {
            File.Delete(Application.persistentDataPath + "/gameData.dat");
            File.Delete(Application.persistentDataPath + "/playerData");
        }
        GameManager gm = FindObjectOfType<GameManager>();
        if (gm != null) gm.clearLocals();
        SceneManager.LoadScene(1);
    }

    //Continues from last saved level
    public void Continue() {
        FindObjectOfType<GameManager>().loadGame();
    }

    public void Options() {
        optionsOpen = !optionsOpen;
        optionsMenu.SetActive(optionsOpen);
    }

    public void volume() {
        AudioListener.volume = volumeSlider.value;
        print(AudioListener.volume);
    }

    public void Exit() {
        Application.Quit();
    }
}
