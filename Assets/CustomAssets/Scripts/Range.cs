﻿using UnityEngine;
using System.Collections;

public class Range : MonoBehaviour {

    EnemyRanged enemy = null;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player"))
        {
            if (enemy == null) {
                enemy = GetComponentInParent<EnemyRanged>();
                enemy.player = other.GetComponent<Player>();
            }

            if (enemy.attackTimer <= 0)
            {
                GetComponentInParent<EnemyRanged>().attacksHit++;
            }
        }
    }
}
