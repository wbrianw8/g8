﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {


    Rigidbody2D rigidBody;
    PlayerAttack attack;
    Vector2 start;
    Vector2 endPos;
    int direction;

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        attack = FindObjectOfType<PlayerAttack>();
        direction = GameObject.FindObjectOfType<Player>().getDirection() * -1;

        start = rigidBody.transform.position;
        endPos = new Vector2(start.x + (3 * direction), start.y);
        rigidBody.gravityScale = 0;
        rigidBody.velocity = new Vector2(5 * direction, 0);

    }

    // Update is called once per frame
    void Update () {
        if (direction > 0)
        {
            if (endPos.x - rigidBody.position.x < 0) DestroyImmediate(gameObject);
        }
        else {
            if (endPos.x - rigidBody.position.x > 0) DestroyImmediate(gameObject);
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.isTrigger)
        {
            IDamagable enemy = (IDamagable)collision.collider.gameObject.GetComponent(typeof(IDamagable));
            if (enemy != null)
            {
                enemy.Damage(attack.getDamage());
            }
            Destroy(gameObject);
        }
    }

}
