﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    public GameObject spawn;
    public int delay;

    private float cooldown;


	// Use this for initialization
	void Start () {
        cooldown = delay;
	}
	
	// Update is called once per frame
	void Update () {
        if (cooldown <= 0)
        {
            Instantiate(spawn, transform.position, Quaternion.identity);
            cooldown = delay;
        }
        else {
            cooldown -= Time.deltaTime;
        }
	}
}
