﻿using UnityEngine;

public class EnemyRanged : EnemyMelee {

    public float attackCooldown;
    public string projectileFileName;
    private GameObject projectile;
    public float offsetPercentage = 1;
    public bool meleeDamage = false;

    private BoxCollider2D enemyBox;
    private Animator anim;
    private float spawnOffset;

    // Use this for initialization
    new void Start () {
        base.Start();
        curHealth = maxHealth;
        attackCd = attackCooldown;
        enemyBox = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        spawnOffset = enemyBox.bounds.extents.x * offsetPercentage;
        projectile = Resources.Load("Prefabs/" + projectileFileName) as GameObject;
    }
	
	// Update is called once per frame
	new void Update () {
        base.Update();
        if (curHealth <= 0) {
            die();
        }
	}

    new void die() {
        base.die();
        if (name.Contains("BossBot")) {
            Cutscene2 cs = FindObjectOfType<Cutscene2>();
            cs.transform.position = transform.position;
            cs.GetComponent<SpriteRenderer>().enabled = true;
            cs.enabled = true;
        }
        Destroy(GetComponentInChildren<Range>());
    }

    public override void attack() {
        attackTimer -= Time.deltaTime;

        if (attacksHit > 0)
        {
            if ((isFacingRight() && player.transform.position.x < gameObject.transform.position.x) || (!isFacingRight() && player.transform.position.x > gameObject.transform.position.x))
            {
                Flip();
            }
            anim.SetTrigger("Attack");
            if (enemyBox.IsTouching(player.GetComponent<BoxCollider2D>())) player.applyDamage(damage);
            attacksHit--;
        }


    }


    void spawnProjectile() {
        //Makes sure projectile is facing the right way
        Quaternion rotation;
        int direction = -1;
        if (isFacingRight()) {
            rotation = Quaternion.Euler(0, 180, 0);
            direction = 1;
        }
        else rotation = Quaternion.Euler(0, 0, 0);


        GameObject project = Instantiate(projectile, new Vector2(transform.position.x + spawnOffset * 3 * direction, transform.position.y), rotation) as GameObject;
        project.GetComponent<nonmovingProjectile>().setDamage(damage);
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        toggleFacingRight();

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }


}
