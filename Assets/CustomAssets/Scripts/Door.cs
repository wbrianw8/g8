﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour {

    public bool next;
    public string levelName;
    public string returnDoorName = null;
    private int nextLevel;
    private Camera2DFollow cam;

	// Use this for initialization
	void Start () {
        if (next) nextLevel = 1;
        else nextLevel = -1;
	}

	void OnTriggerStay2D(Collider2D col) {
        load(col);
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        load(col);
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.CompareTag("Player")) {
            if (cam == null)
            {
                cam = FindObjectOfType<Camera2DFollow>();
            }
            cam.damping = 0.3f;
        }
    }

    void load(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Select") || Input.GetAxis("Vertical") > 0)
            {

                if (cam == null)
                {
                    cam = FindObjectOfType<Camera2DFollow>();
                }
                cam.damping = 0;
                GameManager gm = FindObjectOfType<GameManager>();
                gm.save();
                gm.saveOnLoad(true);
                int lastScene = SceneManager.GetActiveScene().buildIndex;
                gm.load(lastScene, returnDoorName);
                if (levelName != null && levelName.Length > 0) {
                    SceneManager.LoadScene(levelName);
                }
                else {
                    print(SceneManager.GetActiveScene().buildIndex + nextLevel);
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + nextLevel);
                }

                
            }
        }
    }

}
