﻿using UnityEngine;
using System.Collections;
using System;

public class Pickup : MonoBehaviour {

    private Player player;
    private bool pickedUp = false;
    public string type;
    public string itemName;
    public Sprite itemSprite;

	// Use this for initialization
	void Start () {
        GetComponent<SpriteRenderer>().sprite = itemSprite;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        BoxCollider2D itemCollider = null;
        BoxCollider2D playerCollider = null;

        foreach (BoxCollider2D col in GetComponents<BoxCollider2D>()) {
            if (!col.isTrigger) {
                itemCollider = col;
            }
        }
        foreach (BoxCollider2D col in player.GetComponents<BoxCollider2D>())
        {
            if (!col.isTrigger)
            {
                playerCollider = col;
            }
        }

        Physics2D.IgnoreCollision(playerCollider, itemCollider);
	}
	
	// Update is called once per frame
	void Update () {
       /* Vector2 velocity = rigid.velocity;
        if (velocity.y == 0) {
            rigid.velocity = new Vector2(0, 1);
        }*/
	}

    void OnTriggerStay2D(Collider2D col)
    {
        playerEnter(col);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        playerEnter(col);
    }

    void playerEnter(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Pickup") && !pickedUp)
            {
                pickedUp = true;
                player.pickup(this);
                transform.position = Vector2.MoveTowards(transform.position, player.transform.position, 1);
                Destroy(gameObject);
            }
        }
    }

    public string getType() {
        return type;
    }

    public string getName() {
        return itemName;
    }




}
[Serializable]
public class inventoryItem{
    
    private int amount;
    private string name;

    public inventoryItem(string itemName) {
        name = itemName;
        amount = 1;
    }

    public void add() {
        amount++;
    }

    public void remove(int num) {
        amount -= num;
    }

    public string getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }
} 
