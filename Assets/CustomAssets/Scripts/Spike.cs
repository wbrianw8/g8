﻿using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour {


	public int damage = 10;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if(coll.collider.CompareTag("Player")){
			Player player = coll.collider.GetComponent<Player> ();
			player.applyDamage (damage);
			Rigidbody2D playerbody = coll.collider.GetComponent<Rigidbody2D> ();
			playerbody.velocity = new Vector2 (0, 0);
			playerbody.AddForceAtPosition(new Vector2 (1000 * getDirection(playerbody), 100), coll.contacts[0].point);
		}else if(coll.collider.CompareTag("Enemy")){
			Enemy enemy = coll.collider.GetComponent<Enemy>();
			enemy.Damage(damage);
			Rigidbody2D enemybody = enemy.GetComponent<Rigidbody2D> ();
			enemybody.velocity = new Vector2 (0, 0);
			enemybody.AddForceAtPosition(new Vector2 (1000 * getDirection(enemybody), 100), coll.contacts[0].point);
		}
	}

	int getDirection(Rigidbody2D rigid){
		if (rigid.position.x < this.transform.position.x)
			return -1;
		else return 1;
	}
}
